#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <FirebaseESP8266.h>

int contconexion = 0;
unsigned long previousMillis = 0;

char ssid[50];      
char pass[50];

const char *ssidConf = "ESP8266";
const char *passConf = "12345678";
// Se puede sacar?

void guardar_conf();
void grabar(int, String);
void escanear();

String mensaje = "";

#define RELE 5
#define ADC_VREF_mV    5000.0
#define ADC_RESOLUTION 4096.0
#define PIN_LM35 0
#define LED 15

#define FIREBASE_HOST "esp8266-1fe94-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "AIzaSyCeaFjJrZcv0BqeKhN8TkaiWeElQagdcK0"

int tempC;

FirebaseData FIREBASE;

String ruta = "Sensor" ;
String rutaEstado = "OnOff/Estado/" ;

//-----------CODIGO HTML PAGINA DE CONFIGURACION---------------
String pagina = "<!DOCTYPE html>"
"<html lang='en'>"
"<head>"
    "<meta charset='UTF-8'>"
   "<meta http-equiv='X-UA-Compatible' content='IE=edge'>"
    "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"
    "<title>Prueba</title>"
"</head>"
"<body style='display: flex; justify-content: center; align-items: center; flex-direction: column;'>"
"<div style='display: flex; justify-content: center; align-items: center; flex-direction: column;'>"

        "<form action='guardar_conf' method='get' style='display: flex; justify-content: center; align-items: center; flex-direction: column; width: 100vw;'>"
            "<p style='font-size: 25px;'>SSID</p>"
            "<input class='input1' name='ssid' type='text' style='border: none; border-bottom: 2px solid black; height: 25px; width: 40%;'>"
            "<p style='font-size: 25px;'>PASSWORD</p>"
            "<input class='input1' name='pass' type='password' style='border: none; border-bottom: 2px solid black;height: 25px; width: 40%;'> <br> <br>"
            "<input class='boton' type='submit' value='GUARDAR'style='align-items: center;appearance: none;background-color: #fff;border-radius: 24px;border-style: none;box-shadow: rgba(0, 0, 0, .2) 0 3px 5px -1px,rgba(0, 0, 0, .14) 0 6px 10px 0,rgba(0, 0, 0, .12) 0 1px 18px 0;box-sizing: border-box;color: black;cursor: pointer;display: inline-flex;fill: currentcolor;font-size: 17px;font-weight: 500;height: 48px;justify-content: center;letter-spacing: .25px;line-height: normal;max-width: 100%; overflow: visible;padding: 2px 24px;position: relative;text-align: center;text-transform: none;transition: box-shadow 280ms cubic-bezier(.4, 0, .2, 1),opacity 15ms linear 30ms,transform 270ms cubic-bezier(0, 0, .2, 1) 0ms;user-select: none;-webkit-user-select: none;touch-action: manipulation;width: auto;will-change: transform,opacity;z-index: 0;'/> <br>"
"</form>"
"<a href='escanear' style='text-decoration: none;'><button class='boton'style='align-items: center;appearance: none;background-color: #fff;border-radius: 24px;border-style: none;box-shadow: rgba(0, 0, 0, .2) 0 3px 5px -1px,rgba(0, 0, 0, .14) 0 6px 10px 0,rgba(0, 0, 0, .12) 0 1px 18px 0;box-sizing: border-box;color: black;cursor: pointer;display: inline-flex;fill: currentcolor;font-size: 17px;font-weight: 500;height: 48px;justify-content: center;letter-spacing: .25px;line-height: normal;max-width: 100%; overflow: visible;padding: 2px 24px;position: relative;text-align: center;text-transform: none;transition: box-shadow 280ms cubic-bezier(.4, 0, .2, 1),opacity 15ms linear 30ms,transform 270ms cubic-bezier(0, 0, .2, 1) 0ms;user-select: none;-webkit-user-select: none;touch-action: manipulation;width: auto;will-change: transform,opacity;z-index: 0;'>ESCANEAR</button></a><br><br>"
 "</div>"

 ;

String paginafin =
"</body>"
"</html>";

String paginaDiv = "";
String paginaDivFin = "";

//------------------------SETUP WIFI-----------------------------
void setup_wifi() { // Para cuando entra sin tener pulsado el boton de modo config.
// Conexión WIFI
  WiFi.mode(WIFI_STA); // Para que no inicie el SoftAP en el modo normal
  WiFi.begin(ssid, pass); // ssid y pass variables ya guardadas anteriormente en la EEPROM.
  while (WiFi.status() != WL_CONNECTED and contconexion <50) { //Cuenta hasta 50 si no se puede conectar lo cancela
    ++contconexion;
    delay(250);
    Serial.print(".");
    digitalWrite(13, HIGH);
    delay(250);
    digitalWrite(13, LOW);
  }
  if (contconexion <50) {   
      Serial.println("");
      Serial.println("WiFi conectado");
      Serial.println(WiFi.localIP());
      digitalWrite(13, HIGH);  
  }
  else { 
      Serial.println("");
      Serial.println("Error de conexion");
      digitalWrite(13, LOW);
  }
}

//--------------------------------------------------------------
WiFiClient espClient; // Iniciamos un cliente ESP.
ESP8266WebServer server(80); // Iniciamos un WebServer en el puerto 80.
//--------------------------------------------------------------

//-------------------PAGINA DE CONFIGURACION--------------------
void paginaconf() { // Generamos pagina de inicio.
  server.send(200, "text/html", pagina + paginaDiv + mensaje + paginaDivFin + paginafin); // server.send devuelve datos al cliente actualmente conectado. Parametros: (Codigo de respuesta = 200 para OK y 404 para Not Found, Tipo de contenido = "text/html", Contenido = página). 
}

//--------------------MODO_CONFIGURACION------------------------
void modoconf() {
   
  delay(100);
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  
  WiFi.mode(WIFI_AP);
  WiFi.softAP("Setup Portal", "123456789"); // SoftAP setea configuración AP. ***Probar con modo AP o STA_AP***.

  // Ver IP en el monitor.
  IPAddress myIP = WiFi.softAPIP(); 
  Serial.print("IP del acces point: ");
  Serial.println(myIP);
  Serial.println("WebServer iniciado...");

// Inicia páginas del server. Cuando el cliente entre a alguna de las rutas especificadas se llamara a la respectiva función.

  server.on("/", paginaconf); // Esta es la página de configuración.

  server.on("/guardar_conf", guardar_conf); // Graba en la EEPROM la configuración. 

  server.on("/escanear", escanear); // Escanean las redes WiFi disponibles.
  
  server.begin();

  while (true) {
      server.handleClient(); // Manejo de solicitudes de clientes entrantes.
  }
}

//---------------------GUARDAR CONFIGURACION-------------------------
void guardar_conf() {
  
  Serial.println(server.arg("ssid")); //Recibimos los valores que envia por GET el formulario web.
  grabar(0,server.arg("ssid"));
  Serial.println(server.arg("pass"));
  grabar(50,server.arg("pass"));

  mensaje = "Configuracion Guardada...";
  // Se imprime de nuevo la pagina con el mensaje.
  paginaconf();
  
}

//----------------Función para grabar en la EEPROM-------------------
void grabar(int addr, String a) { // Dos parametros: (Un entero que va a ser la dirrección, Y un string que es lo que se va a guardar). 
  int tamano = a.length(); // Define el tamaño del string a.
  char inchar[50]; // Genera una cadena de 50 caracteres.
  a.toCharArray(inchar, tamano+1); // toCharArray extrae los caracteres de una cadena en una matriz de caracteres. *** +1 Pq va de 0 a 50 ***
  for (int i = 0; i < tamano; i++) { // Un for que va desde el 0 hasta el tamaño del string a, sumando de uno en uno.
    EEPROM.write(addr+i, inchar[i]); // Graba en la EEPROM caracter por caracter. ***
  }
  for (int i = tamano; i < 50; i++) { // Cuandp recorre todo el string y el tamaño es menor a los 50 caracteres. ***
    EEPROM.write(addr+i, 255); // Cuando en la EEPROM no hay nada los bytes son de 255. ***
  }
  EEPROM.commit(); // Hace un commit.
}

//-----------------Función para leer la EEPROM------------------------
String leer(int addr) {
   byte lectura;
   String strlectura;
   for (int i = addr; i < addr+50; i++) {
      lectura = EEPROM.read(i);
      if (lectura != 255) {
        strlectura += (char)lectura;
      }
   }
   return strlectura;
}

//---------------------------ESCANEAR----------------------------
void escanear() {  
  int n = WiFi.scanNetworks(); // Devuelve el número de redes encontradas
  Serial.println("escaneo terminado");
  if (n == 0) { // Si no encuentra ninguna red
    Serial.println("No se encontraron redes");
    mensaje = "No se encontraron redes";
  }  
  else
  {
    Serial.print(n);
    Serial.println(" Redes encontradas");
    mensaje = "";
    for (int i = 0; i < n; ++i)
    {
      paginaDiv =  "<div style='width: 70vw; border: solid; border-color: black; border-width: 3px; display: flex; justify-content: center; flex-direction: column; align-items: center;'>" ;
      paginaDivFin = "</div>";
      // Agrega al STRING "mensaje" la información de las redes encontradas 
      mensaje = (mensaje) + "<p style='font-size: 15px; width: 100%; margin-left: 10px;'>" + String(i + 1) + ": " + WiFi.SSID(i) + " (" + WiFi.RSSI(i) + ")" + " </p>\r\n";
      delay(10);
    }
    Serial.println(mensaje);
    paginaconf();
  }
}

//------------------------SETUP-----------------------------
void setup() {

  pinMode(13, OUTPUT);
  pinMode(RELE, OUTPUT);
  pinMode(LED, OUTPUT);
  Serial.println(FIREBASE.intData());
  
  // Inicia Serial
  Serial.begin(115200);
  Serial.println("");

  EEPROM.begin(512);

  pinMode(14, INPUT);
  if (digitalRead(14) == 0) {
    modoconf();
  }

  leer(0).toCharArray(ssid, 50);
  leer(50).toCharArray(pass, 50);

Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
Firebase.reconnectWiFi(true);

  setup_wifi();
}

//--------------------------LOOP--------------------------------
void loop() {

tempC = analogRead(PIN_LM35);
float millivolts = (tempC/1024.0) * 3300;
tempC = millivolts/100;
Serial.print("Temperatura(°C)=   ");
Serial.println(tempC);
Firebase.setInt(FIREBASE, ruta, tempC);
Firebase.getBool(FIREBASE, rutaEstado);
// Serial.println(FIREBASE.intData());
if (FIREBASE.intData() == 1)
{
  digitalWrite(LED, HIGH);
  digitalWrite(RELE, LOW);
}
else{
  digitalWrite(LED, LOW);
  digitalWrite(RELE, HIGH);
}

}



