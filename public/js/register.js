import { initializeApp } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-app.js";
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase, ref, onValue, set } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-database.js";
import { getAuth, onAuthStateChanged, createUserWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-auth.js";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCeaFjJrZcv0BqeKhN8TkaiWeElQagdcK0",
  authDomain: "esp8266-1fe94.firebaseapp.com",
  projectId: "esp8266-1fe94",
  storageBucket: "esp8266-1fe94.appspot.com",
  messagingSenderId: "405793997283",
  appId: "1:405793997283:web:6fe8ff85efa3860f09a0d6"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();
const user = auth.currentUser;

let correoRefReg = document.getElementById("InputEmail2")
let passRefReg = document.getElementById("InputPassword2")
let buttonRefReg = document.getElementById("buttonRegister")
let NombreRefReg = document.getElementById("InputName2");

buttonRefReg.addEventListener("click", RegistroUser);

function RegistroUser(){

  if((correoRefReg.value != '') && (passRefReg.value != '') && (NombreRefReg.value != '')){

    createUserWithEmailAndPassword(auth, correoRefReg.value, passRefReg.value, NombreRefReg.value)
    .then((userCredential) => {
    // Signed in 
      const user = userCredential.user;
      console.log("Usuario: " + user + " ID: " + user.uid);
      console.log("Creación de usuario.");

      let nombre = NombreRefReg.value
      let correo = correoRefReg.value

      set(ref(database, "usuarios/" + user.uid), {
        Nombre_Apellido: nombre,
        Correo: correo
      }).then(() => {
        window.location.href = "../index.html";
      })
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
      alert(errorMessage);
    });
  }
  else{
    alert("Revisar que los campos de usuario y contraseña esten completos.");
  }  
}
