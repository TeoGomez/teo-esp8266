import { initializeApp } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase, ref, onValue } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-database.js";
import { getAuth, onAuthStateChanged, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-auth.js";
// Your web app's Firebase configuration

const firebaseConfig = {
    apiKey: "AIzaSyCeaFjJrZcv0BqeKhN8TkaiWeElQagdcK0",
    authDomain: "esp8266-1fe94.firebaseapp.com",
    projectId: "esp8266-1fe94",
    storageBucket: "esp8266-1fe94.appspot.com",
    messagingSenderId: "405793997283",
    appId: "1:405793997283:web:6fe8ff85efa3860f09a0d6"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();
const user = auth.currentUser;

let correoRefLog = document.getElementById("InputEmail1")
let passRefLog = document.getElementById("InputPassword1")
let buttonRefLog = document.getElementById("buttonLogin")

buttonRefLog.addEventListener("click", LogIn);

//funcion para Iniciar sesion

function LogIn (){

if((correoRefLog.value != '') && (passRefLog.value != '')){

    signInWithEmailAndPassword(auth, correoRefLog.value, passRefLog.value)
    .then((userCredential) => {
        const user = userCredential.user;
        window.location.href = "../index.html";
    })
    .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        alert(errorMessage);
    });
}
else{
    alert("Revisar que los campos de usuario y contraseña esten completos.");
}    

}