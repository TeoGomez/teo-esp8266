import { initializeApp } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase, ref, onValue, set } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-database.js";
import { getAuth, onAuthStateChanged, signOut } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-auth.js";
// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCeaFjJrZcv0BqeKhN8TkaiWeElQagdcK0",
    authDomain: "esp8266-1fe94.firebaseapp.com",
    projectId: "esp8266-1fe94",
    storageBucket: "esp8266-1fe94.appspot.com",
    messagingSenderId: "405793997283",
    appId: "1:405793997283:web:6fe8ff85efa3860f09a0d6"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();
const user = auth.currentUser;

var correo;
var UID;

let powerButtonRef = document.getElementById ("powerButton");
let navRef = document.getElementById("navId");
let sistemaRef = document.getElementById("sistemaId");
let tempRef = document.getElementById("temp");

onAuthStateChanged(auth, (user) => {
  if (user) {
    const uid = user.uid;
    UID = uid;
    console.log("Usuario actual:" + uid);

    const email = user.email;
    correo=email;
    console.log(email)
    powerButtonRef.addEventListener("click", EnviarTrueFalse)
    navRef.innerHTML = `
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="#" style="color: white;">Home</a>
  </li>
</li>
<li class="nav-item">
    <button type="button" id="logout" style="background-color:#243b55; color: white;" >Cerrar sesion</button>
</li>
    `
    let logoutRef = document.getElementById("logout")
    logoutRef.addEventListener("click", Logout)
  } else {
    console.log("Ningun usuario detectado ")
    powerButtonRef.addEventListener("click", Ventana)
  }
});

function Ventana(){
  window.location.href = "pages/login.html"
}

function Logout(){
  signOut(auth).then(() => {
    // Sign-out successful.
    location.reload()
  }).catch((error) => {
    // An error happened.
  });
}

const OnOffRef = ref(database, '/OnOff/Estado');
onValue(OnOffRef, (snapshot) => {
  const dataOnOff = snapshot.val();
  if (dataOnOff == true){
    var dataOnOFF1 = "Estado: Encendido"
  }
  else{
    var dataOnOFF1 = "Estado: Apagado"
  }
  sistemaRef.innerHTML = `
  <br><p style="color: #03e9f4;">${dataOnOFF1}</p>
  `;
})

const RutaRef = ref(database, 'Sensor/');
onValue(RutaRef, (snapshot) => {
  const data = snapshot.val();
  tempRef.innerHTML = `
  <p style="font-size: 100px; color: #03e9f4;">${data + "°C"}</p>
  `;
})

function EnviarTrueFalse(){
  const OnOffRef = ref(database, '/OnOff/Estado');
  onValue(OnOffRef, (snapshot) => {
    const dataOnOff = snapshot.val();
    if (dataOnOff == true){
      set(ref(database, "OnOff/"), {
          Estado: false,
      })
    }
    else{
      set(ref(database, "OnOff/"), {
          Estado: true,
      })
    }
  })
}